import {Component } from '@angular/core';
import { Day } from "./model/day.model";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']

})
export class AppComponent {

    private _days: string[] = ['Monday', 'Tuesday', "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    private _periods: string[] = ['Morning', 'Afternoon', 'Evening'];

    firstDay: number = 0;
    numberDays: number = 7;
    weekDays: Day[] = [];
    numberPeriods: number = 3;
    firstPeriod: number = 0;
    periods: any[] = [];

    currentEvent: any;
    events: any[] = [];
    eventTitle: string;

    cursor = {
        x: 0,
        y: 0,
        incrementX: 0,
        incrementY: 0
    };


    constructor(
    ){
        this.RecalculateDays();
        this.RecalculatePeriods();
    }


    RecalculateDays(){
        this.weekDays = [];
        let x: number = 0;
        let index: number;

        while( x < this.numberDays){
            index = +x + +this.firstDay;
            if(index > 6){
                index = +index - 7;
            }
            let day = new Day();
            day.id = index;
            day.name = this._days[index];

            this.weekDays.push(day);
            x ++;
        }
        $('#tooltip').hide();
    }

    RecalculatePeriods(){
        this.periods = [];
        let index: number;
        for(let x = 0; x < this.numberPeriods; x++) {
            index = x + +this.firstPeriod;
            if(typeof this._periods[index] != 'undefined'){
                this.periods.push({
                    index: index,
                    name: this._periods[index]
                });
            }
        }
        this.numberPeriods = this.periods.length;
        $('#tooltip').hide();
    }

    TooltipShow(e){
        let htmlElem = $(e.target);
        let tooltip = $('#tooltip');

        // Setup current day identifiers
        this.currentEvent = {
            startDayId: $(htmlElem).attr('data-day'),
            startPeriodId: $(htmlElem).attr('data-period')
        };

        let elWidth = htmlElem.width();
        let elHeight = htmlElem.height();
        let position = htmlElem.position();
        let margin = 5;

        let toHeight = tooltip.height();
        let toLeft = position.left + elWidth + margin;
        let toTop = position.top - ((toHeight - elHeight)/2);

        let style = {
            display: 'block',
            left: toLeft + 'px',
            top: toTop + 'px'
        };

        tooltip.css(style);
    }

    SaveEvent() {
        if(typeof this.eventTitle != 'undefined' && this.eventTitle != ''){
            let event = {
                startDayId: +this.currentEvent.startDayId,
                startPeriodId: +this.currentEvent.startPeriodId,
                periodCount: 1,
                dayCount: 1,
                text: this.eventTitle
            };
            this.events.push(event);
            this.eventTitle = '';
        }

        setTimeout(function() {
            this.jQueryEvents();
            $('#tooltip').hide();
        }.bind(this),100);
    }

    ExistEvent(dayId, periodId){
        let exist = false;
        this.events.forEach(function(item, index){
            if(item.startDayId == dayId && item.startPeriodId == periodId){
                exist = item.text;
            }
        });

        return exist;
    }

    jQueryEvents(){
        let that = this;
        let target;

        $('.grab').mousedown(function(event){
            that.cursor.x = event.pageX;
            that.cursor.y = event.pageX;

            target = event.target;
        });

        $(document).mouseup(function(e){
            target = null;
            $('.resizing').removeClass('resizing');
        });

        $('.grab').mouseup(function(){
            setTimeout(() => { $('#tooltip').hide(); },100);
        });

        $('body').mousemove(function( event ) {
            if(target){
                that.cursor.incrementX = event.pageX - that.cursor.x;
                that.cursor.incrementY = event.pageY - that.cursor.y;
                that.cursor.x = event.pageX;
                that.cursor.y = event.pageY;
                let box = $(target).parent();
                let finalHeight = box.outerHeight() + that.cursor.incrementY;

                box.css({
                    height: finalHeight + 'px'
                }).addClass('resizing');
            }
        }.bind(this));
    }

    Submit(){
        alert('Submit data. Checkout the console.');
        console.log(this.events);
    }
}
