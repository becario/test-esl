interface data {
    id: number;
    name: string;
    periodCount: number;
}

export class Day implements data {
    constructor (public id = null,
                 public name = '',
                 public periodCount = 3
    ) {

    }
}