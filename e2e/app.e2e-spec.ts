import { AppEslPage } from './app.po';

describe('app-esl App', function() {
  let page: AppEslPage;

  beforeEach(() => {
    page = new AppEslPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
